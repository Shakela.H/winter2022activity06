import java.util.Random;
public class Die{
	private int pips;
	private Random r;
	
	//constructor
	public Die(){
		this.pips = 1;
		this.r = new Random();
	}
	//getMethods
	public int getPips(){
		return this.pips;
	}
	
	public Random getR(){
		return this.r;
	}
	
	//method
	public int roll(){
		this.pips = r.nextInt(6)+1;
		return this.pips;
	}
	//toString
	public String toString(){
		return "Pips: " + this.pips;
	}
}
	