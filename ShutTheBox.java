public class ShutTheBox{
	public static void main(String [] args){
		System.out.println("Welcome to the Shut The Box Game!");
		//Board object called game
		Board game = new Board();
		//initializing boolean gameOver
		boolean gameOver = false;
		//loop, while its true, it will loop
		while (!(gameOver==true)){
			System.out.println("Player 1's turn. Your Board: "+game.toString());
			System.out.println("");
			//calling playATurn from the board object
			game.playATurn();
			//player 1
			if (game.playATurn()==true){
				System.out.println("Player 2 wins!");
				gameOver = true;
			}else{
				System.out.println("Player 1's board: "+game.toString());
				System.out.println("GameOver. Player 2's turn ");
				gameOver = true;
			}
			//player 2
			System.out.println("");
			System.out.println("Player 2's turn. Your Board: "+game.toString());
			game.playATurn();
			if (game.playATurn()==true){
				System.out.println("Player 1 wins!");
				gameOver = true;
			}else{
				System.out.println("Player 2's board: "+game.toString());
				System.out.println("GameOver.");
				gameOver = true;
			}
		}
	}
}