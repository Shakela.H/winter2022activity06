public class Board{
	private Die dice1;
	private Die dice2;
	private boolean[] closedTiles;
	
	
	//constructor
	public Board(){
		this.dice1 = new Die();
		this.dice2 = new Die();
		//set boolean array to all false, later going to update when needed.
		this.closedTiles = new boolean[]{false,false,false,false,false,false,false,false,false,false,false,false};	
		
	}
	public String toString(){
		String value="";
		for(int i = 0; i<this.closedTiles.length; i++){
			if(this.closedTiles[i]==false){
				//any advice how to makes these 4 lines of code less messy?
				int num = i+1;
				value += num +" ";
			}else{
				value += "X ";
			}
		}
		return value;
	}

	public boolean playATurn(){
		//calling roll method on the dice1 and dice2
		this.dice1.roll();
		this.dice2.roll();
		//System.out.println(this.dice1.toString() + "," +this.dice2.toString());
		int sumOfDice = this.dice1.roll()+this.dice2.roll();
		//checking tile position if it matches with the sum of the 2 die.
		if(this.closedTiles[sumOfDice-1]==false){
			this.closedTiles[sumOfDice-1]=true;
			System.out.println("");
			System.out.println("Rolling dice...");
			System.out.println(this.dice1.toString() + "," +this.dice2.toString());
			System.out.println("Closing tile: " +sumOfDice);
			System.out.println("");
			return false;
		}else{
			System.out.println("Tile already shut");
			return true;
		}
	}
}
